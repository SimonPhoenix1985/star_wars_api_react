import {
    GET_ROOT_SUCCESS,
    SET_ID,
    SET_ACTIVE_CATEGORY,
    GET_CATEGORY_SUCCESS,
    GET_DETAILS_SUCCESS,
} from '../config';

const initialState = {
    root: {},
    activeCategory: '',
    id: null,
    categories: {},
    details: {},
}

export default function properties(state = initialState, action) {
    switch (action.type) {
        case GET_ROOT_SUCCESS: {
            return {
                ...state,
                root: action.payload,
            };
        }
        case SET_ID: {
            return {
                ...state,
                id: action.payload,
            };
        }
        case SET_ACTIVE_CATEGORY: {
            return {
                ...state,
                activeCategory: action.payload,
            };
        }
        case GET_CATEGORY_SUCCESS: {
            return {
                ...state,
                categories: {
                    ...state.categories,
                    [action.category]: {
                        ...state.categories[action.category],
                        [action.page]: action.payload,
                    },
                }
            };
        }

        case GET_DETAILS_SUCCESS: {
            return {
                ...state,
                details: {
                    ...state.details,
                    [action.category]: {
                        ...state.details[action.category],
                        ...action.payload
                    }
                },
            };
        }

        default:
            return state;

    }
}

