import { INIT_ROUTER_HISTORY, ADD_ROUTER_HISTORY } from '../config';

const initialState = {
    history: null,
    routingHistory: [],
    previousLocation: null,
};

export default function map(state = initialState, action) {
    switch (action.type) {
        case INIT_ROUTER_HISTORY:
            return {
                ...state,
                history: action.payload,
                routingHistory: [action.payload.location],
            };
        case ADD_ROUTER_HISTORY:
            return {
                ...state,
                previousLocation: state.routingHistory[state.routingHistory.length - 1],
                routingHistory: [...state.routingHistory, action.payload],
            };
        default:
            return state;
    }
}
