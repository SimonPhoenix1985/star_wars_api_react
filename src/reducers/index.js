import { combineReducers } from 'redux';
import router from './router';
import api from './api';

export default combineReducers({
    router,
    api,
});
