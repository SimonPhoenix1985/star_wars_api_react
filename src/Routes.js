import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Route, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import Root from './components/Root';
import Category from './components/Category';
import Details from './components/Details';
import { bindActionCreators } from 'redux';
import { loadRoot } from './actions/api';

class Routes extends Component {
    static propTypes = {
        root: PropTypes.shape({}).isRequired,
        loadRootAction: PropTypes.func.isRequired,
    }
    async componentDidMount() {
        const { loadRootAction } = this.props;
        await loadRootAction();
    }

    getPath = (id = null) => {
        const { root } = this.props;
        return Object.keys(root).map(path => `/${path}${id ? '/:id' : ''}`);
    }

    render() {
        return (
            <div>
                <Root />
                <PublicRoute path={this.getPath()} exact component={Category} />
                <PublicRoute path={this.getPath('id')} exact component={Details} />
            </div>
        );
    }
}
const isBlocked = () => localStorage.getItem('user_blocked');
const PublicRoute = ({ component: RenderComponent, ...rest }) => (
    <Route
        {...rest}
        render={props => (isBlocked() ? (
            <Redirect
                to={{
                    pathname: '/',
                    state: { from: props.location },
                }}
            />
        ) : (
            <RenderComponent {...props} />
        ))
        }
    />
);
const mapStateToProps = state => ({
    root: state.api.root,
});

const mapDispatchToProps = dispatch => bindActionCreators({
    loadRootAction: loadRoot,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Routes);
