import { API } from '../config';
export const getParamFromURL = (url, params) => {
    const urlObj = new URL(url);
    if (Array.isArray(params)) {
        const res = {};
        params.forEach(param => {
            res[param] = urlObj.searchParams.get(param);
        });

        return res;
    }
    return urlObj.searchParams.get(params);
};

export const getDataFromURL = (url, short = false) => {
    if (short) {
        const splitUrl = url.split('/');
        return { category: splitUrl[1], id: splitUrl[2] };
    }
    const splitUrl = url.split(`${API}`)[1];
    const category = splitUrl.split('/')[0];
    const id = splitUrl.split('/')[1];
    return { category, id };
};
