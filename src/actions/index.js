import { API } from '../config';
export default async function fetchApi (endpoint = '', api = API, method = 'GET', data, content = 'application/json') {
    const headers = {
        Authorization: sessionStorage.getItem('token'),
        'Content-Type': content,
    };

    const res = await fetch(`${api}${endpoint}`, {
        method,
        headers,
        body: JSON.stringify(data),
    });

    const jsonData = await res.json();

    return jsonData;
};
