import fetchApi from '.';
import store from '../store';
import {
    GET_ROOT_REQUEST,
    GET_ROOT_SUCCESS,
    GET_ROOT_FAIL,
    SET_ACTIVE_CATEGORY,
    GET_CATEGORY_REQUEST,
    GET_CATEGORY_SUCCESS,
    GET_CATEGORY_FAIL,
    GET_DETAILS_REQUEST,
    GET_DETAILS_SUCCESS,
    GET_DETAILS_FAIL,
} from '../config';
import { getDataFromURL } from '../helpers/helpers';

const getRootRequest = () => ({
    type: GET_ROOT_REQUEST,
});

const getRootSuccess = payload => ({
    type: GET_ROOT_SUCCESS,
    payload,
});

const getRootFail = payload => ({
    type: GET_ROOT_FAIL,
    payload,
});

const setActiveCategoryAction = payload => ({
    type: SET_ACTIVE_CATEGORY,
    payload,
});

const getCategoryRequest = payload => ({
    type: GET_CATEGORY_REQUEST,
    payload,
});

const getCategorySuccess = (category, page, payload) => ({
    type: GET_CATEGORY_SUCCESS,
    category,
    page,
    payload,
});

const getCategoryFail = payload => ({
    type: GET_CATEGORY_FAIL,
    payload,
});

const getDetailsRequest = payload => ({
    type: GET_DETAILS_REQUEST,
    payload,
});

const getDetailsSuccess = (category, payload) => ({
    type: GET_DETAILS_SUCCESS,
    category,
    payload,
});

const getDetailsFail = payload => ({
    type: GET_DETAILS_FAIL,
    payload,
});

export const loadRoot = () => async dispatch => {
    dispatch(getRootRequest());
    try {
        const root = store.getState().api.root;
        if (!Object.keys(root).length) {
            const res = await fetchApi();
            dispatch(getRootSuccess(res));
        }
    }
    catch (error) {
        dispatch(getRootFail(error));
    }
};

export const loadCategory = (category, page = '') => async dispatch => {
    dispatch(getCategoryRequest(category));
    const categories = store.getState().api.categories;
    if (!categories[category] || !categories[category][page]) {
        try {
            const res = await fetchApi(`${category}${page ? `/?page=${page}` : ''}`);
            const details = store.getState().api.details;
            const newDetails = {};
            res.results.forEach(result => {
                const { id } = getDataFromURL(result.url);
                if (!details[category] || !details[category][id]) {
                    newDetails[id] = result;
                }
            });
            if (Object.keys(newDetails).length) {
                dispatch(getDetailsSuccess(category, newDetails));
            }
            dispatch(getCategorySuccess(category, page || 1, res));
        } catch (error) {
            dispatch(getCategoryFail(error));
        }
    }
};

export const setActiveCategory = category => dispatch => (
    dispatch(setActiveCategoryAction(category))
);

export const loadDetails = (category, id) => async dispatch => {
    dispatch(getDetailsRequest(category));
    const details = store.getState().api.details;
    let res = {};
    if (!details[category] || !details[category][id]) {
        try {
            res = await fetchApi(`${category}/${id}`);
        } catch (error) {
            dispatch(getDetailsFail(error));
        }
    } else {
        res = details[category][id];
    }
    const keys = Object.keys(res);
    keys.forEach(async key => {
        if (Array.isArray(res[key])) {
            for (let i = 0; i < res[key].length; i++) {
                const { category: innerCategory, id: innerID } = getDataFromURL(res[key][i]);
                if (!details[innerCategory] || !details[innerCategory][innerID]) {
                    const innerRes = await fetchApi(`${innerCategory}/${innerID}`);
                    dispatch(getDetailsSuccess(innerCategory, { [innerID]: innerRes }));
                }
            }
        }
    });
    dispatch(getDetailsSuccess(category, { [id]: res }));
}