import {
    INIT_ROUTER_HISTORY, ADD_ROUTER_HISTORY,
} from '../config';

export const initRouterHistory = (history) => ({
    type: INIT_ROUTER_HISTORY,
    payload: history,
});

export const addRouterHistory = (location) => ({
    type: ADD_ROUTER_HISTORY,
    payload: location,
});
