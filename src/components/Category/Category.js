import React, { Component } from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import ReactPaginate from 'react-paginate';
import { loadCategory, setActiveCategory } from '../../actions/api';
import { getDataFromURL } from '../../helpers/helpers';
import Cards from '../Cards';

class Category extends Component {
    static propTypes = {
        categories: PropTypes.shape({}).isRequired,
        loadCategoryAction: PropTypes.func.isRequired,
        setActiveCategoryAction: PropTypes.func.isRequired,
        activeCategory: PropTypes.string.isRequired,
        history: PropTypes.shape({}).isRequired,
    }

    state = {
        activeCategory: '',
        currentPage: 1,
    };

    async componentDidMount() {
        await this.init();
    }

    async componentDidUpdate(prevProps) {
        const {
            history: { location: { pathname } },
        } = this.props;
        if (prevProps.location.pathname !== pathname ) {
            await this.init();
        }
    }
    init = async () => {
        const {
            history: { location: { pathname } },
            loadCategoryAction,
            setActiveCategoryAction,
        } = this.props;
        const { category } = getDataFromURL(pathname, 'short');
        setActiveCategoryAction(category);
        await loadCategoryAction(category);
        this.setState({ currentPage: 1 });
    }
    handlePageClick = async ({ selected }) => {
        const nextPage = selected + 1;
        const { loadCategoryAction, activeCategory } = this.props;
        await loadCategoryAction(activeCategory, nextPage);
        this.setState({currentPage: nextPage});
    };

    getTotalPages = () => {
        const { categories, activeCategory } = this.props;
        if (
            categories[activeCategory]
            && categories[activeCategory]['1']
            && categories[activeCategory]['1'].count
        ) {
            return Math.round(categories[activeCategory]['1'].count / 10);
        }
        return 0;
    };

    render() {
        const { history, categories, activeCategory } = this.props;
        const { currentPage } = this.state;
        let cards = [];
        if (categories[activeCategory] && categories[activeCategory][currentPage]) {
            cards = categories[activeCategory][currentPage].results;
        }
        return (
            <Wrapper>
                <Cards
                    cards={cards}
                    history={history}
                />
                <ReactPaginate
                    previousLabel={'previous'}
                    nextLabel={'next'}
                    breakLabel={'...'}
                    breakClassName={'break-me'}
                    pageCount={this.getTotalPages()}
                    marginPagesDisplayed={2}
                    pageRangeDisplayed={5}
                    onPageChange={this.handlePageClick}
                    containerClassName={'pagination'}
                    subContainerClassName={'pages pagination'}
                    activeClassName={'active'}
                />
            </Wrapper>
        );
    }
}
const Wrapper = styled.div`

`;

const mapStateToProps = state => ({
    categories: state.api.categories,
    activeCategory: state.api.activeCategory,
});

const mapDispatchToProps = dispatch => bindActionCreators({
    loadCategoryAction: loadCategory,
    setActiveCategoryAction: setActiveCategory,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Category);
