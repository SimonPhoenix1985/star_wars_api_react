import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { loadDetails } from '../../actions/api';
import { getDataFromURL } from '../../helpers/helpers';
import Cards from '../Cards';

class Details extends Component {
    static propTypes = {
        details: PropTypes.shape({}).isRequired,
        loadDetailsAction: PropTypes.func.isRequired,
        activeCategory: PropTypes.string.isRequired,
        history: PropTypes.shape({}).isRequired,
    }
    state = {
      category: null,
      id: null,
    };

    async componentDidMount() {
        await this.init();
    }

    async componentDidUpdate(prevProps) {
        const {
            history: { location: { pathname } }
        } = this.props;
        if (prevProps.location.pathname !== pathname ) {
            await this.init();
        }
    }

    init = async () => {
        const {
            loadDetailsAction,
            activeCategory,
            history: { location: { pathname } }
        } = this.props;
        const { category, id } = getDataFromURL(pathname, 'short');
        this.setState({ id, category: activeCategory || category });
        await loadDetailsAction(category, id);
    }

    renderValue = value => {
        if (Array.isArray(value)) {
            const { details, history } = this.props;
            const cards = [];
            value.forEach(url => {
                const { category, id } = getDataFromURL(url);
                if (details[category] && details[category][id]) {
                    cards.push(details[category][id]);
                }
            });

            return (
                <Cards
                    cards={cards}
                    history={history}
                />
            );
        }
        return value;
    }

    render() {
        const { details } = this.props;
        const { id, category } = this.state;
        let data = {};
        if (details[category] && details[category][id]) {
            data = details[category][id];
        }
        return (
            <div>
                {
                    Object.keys(data).map((key, index) => (
                        <div key={index}>{key}: {this.renderValue(details[category][id][key])}</div>
                    ))
                }
            </div>
        );
    }
}
const mapStateToProps = state => ({
    details: state.api.details,
    activeCategory: state.api.activeCategory,
});

const mapDispatchToProps = dispatch => bindActionCreators({
    loadDetailsAction: loadDetails,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Details);
