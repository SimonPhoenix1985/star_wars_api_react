import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

const propTypes = {
    root: PropTypes.shape({}).isRequired,
};

const Root = ({
    root,
}) => (
    <div>
        <ul>
            {
                Object.keys(root).map((address, index) => (
                    <li key={index}>
                        <Link
                            to={{ pathname: `/${address}`, state: '/' }}
                        >
                            {address}
                        </Link>
                    </li>

                ))
            }
        </ul>
    </div>
)

Root.propTypes = propTypes;

const mapStateToProps = state => ({
    root: state.api.root,
});

export default connect(mapStateToProps, null)(Root);
