import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { getDataFromURL } from '../../helpers/helpers';

const propTypes = {
    cards: PropTypes.arrayOf(PropTypes.object).isRequired,
    history: PropTypes.shape({}).isRequired,
};

const Cards = ({
    cards, history,
}) => {
    const getDetails = (url) => {
        const { category, id } = getDataFromURL(url);
        history.replace(`/${category}/${id}`);
    };
    return (
        <CardsWrapper>
            {
                cards.map(({ url, name, title }) => (
                    <Card
                        tabIndex="0"
                        role="button"
                        key={url}
                        onClick={() => {
                            getDetails(url);
                        }}
                    >
                        <div>{name || title}</div>
                    </Card>
                ))
            }
        </CardsWrapper>
    );
}

Cards.propTypes = propTypes;

const CardsWrapper = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr 1fr;
  grid-column-gap: 18px;
  grid-row-gap: 10px;
`;

const Card = styled.div`
  background: gray;
  display: flex;
`;

export default Cards;
