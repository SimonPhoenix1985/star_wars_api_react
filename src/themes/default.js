import { createGlobalStyle } from 'styled-components';

const AvenirNextBold = require('../assets/fonts/AvenirNext-Bold-01.ttf');
const AvenirNextBoldItalic = require('../assets/fonts/AvenirNext-BoldItalic-02.ttf');
const AvenirNextDemiBold = require('../assets/fonts/AvenirNext-DemiBold-03.ttf');
const AvenirNextDemiBoldItalic = require('../assets/fonts/AvenirNext-DemiBoldItalic-04.ttf');
const AvenirNextItalic = require('../assets/fonts/AvenirNext-Italic-05.ttf');
const AvenirNextMedium = require('../assets/fonts/AvenirNext-Medium-06.ttf');
const AvenirNextMediumItalic = require('../assets/fonts/AvenirNext-MediumItalic-07.ttf');
const AvenirNextRegular = require('../assets/fonts/AvenirNext-Regular-08.ttf');
const AvenirNextUltraLight = require('../assets/fonts/AvenirNext-UltraLight-11.ttf');
const AvenirNextUltraLightItalic = require('../assets/fonts/AvenirNext-UltraLightItalic-12.ttf');

// eslint-disable-next-line
export const GlobalStyle = createGlobalStyle`
  @import url('https://api.tiles.mapbox.com/mapbox-gl-js/v0.52.0/mapbox-gl.css');
  @import url('https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v2.3.0/mapbox-gl-geocoder.css');

  @font-face {
    font-family: 'Avenir Next';
    font-style: normal;
    font-weight: 100;
    src: url(${AvenirNextUltraLight});
  }

  @font-face {
    font-family: 'Avenir Next';
    font-style: italic;
    font-weight: 100;
    src: url(${AvenirNextUltraLightItalic});
  }

  @font-face {
    font-family: 'Avenir Next';
    font-style: normal;
    font-weight: 400;
    src: url(${AvenirNextRegular});
  }

  @font-face {
    font-family: 'Avenir Next';
    font-style: italic;
    font-weight: 400;
    src: url(${AvenirNextItalic});
  }

  @font-face {
    font-family: 'Avenir Next';
    font-style: normal;
    font-weight: 500;
    src: url(${AvenirNextMedium});
  }

  @font-face {
    font-family: 'Avenir Next';
    font-style: italic;
    font-weight: 500;
    src: url(${AvenirNextMediumItalic});
  }

  @font-face {
    font-family: 'Avenir Next';
    font-style: normal;
    font-weight: 700;
    src: url(${AvenirNextDemiBold});
  }

  @font-face {
    font-family: 'Avenir Next';
    font-style: italic;
    font-weight: 700;
    src: url(${AvenirNextDemiBoldItalic});
  }

  @font-face {
    font-family: 'Avenir Next';
    font-style: normal;
    font-weight: 900;
    src: url(${AvenirNextBold});
  }

  @font-face {
    font-family: 'Avenir Next';
    font-style: italic;
    font-weight: 900;
    src: url(${AvenirNextBoldItalic});
  }

  @media (max-width: 850px) {
    *:focus {
      outline: none;
    }
  }

  body {
    top: 0;
    left: 0;
    bottom: 0;
    right: 0;

    position: ${props => (props.position ? 'static' : 'fixed')};
    
    margin: 0;
    font-family: 'Avenir Next', sans-serif;
    font-weight: 700;
  }

  main {
    padding: 50px 0 60px;
  }

  #root {
    /* min-height: 100vh; */
  }

  ul {
    margin: 0;
    padding: 0;
  }
  a {
    text-decoration: none;
  }
`;
