import { createBrowserHistory } from 'history';

import store from '../store';
import { initRouterHistory, addRouterHistory } from '../actions/router';

const history = createBrowserHistory();
store.dispatch(initRouterHistory(history));

history.listen((location) => {
    store.dispatch(addRouterHistory(location));
});

export default history;
