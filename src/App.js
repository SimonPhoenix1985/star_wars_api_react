import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { Router } from 'react-router-dom';
import Routes from './Routes';

import { GlobalStyle } from './themes/default';

import store from './store';

import history from './services/history';

class App extends Component {
    render() {
        return (
            <Provider store={store}>
                <>
                    <GlobalStyle />
                    <Router history={history}>
                        <Routes />
                    </Router>
                </>
            </Provider>
        );
    }
}

export default App;
